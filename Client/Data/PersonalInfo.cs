﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;

namespace P2PChat.Client.Data
{
    public static class PersonalInfo
    {
        public static string Nickname { get; set; }
        public static string EmailAddress { get; set; }
        public static string TelNumber { get; set; }
        public static int Portrait { get; set; }

        public static Guid UserId { get; set; }
        public static string RegEmail { get; set; }

        //这段代码包含的XML样本:
        //<PersonalInfo GUID="xxx" RegEmail="xxx">
        //  <Nickname>xxx</Nickname>
        //  <EmailAddress>xxx</EmailAddress>
        //  <TelNumber>xxx</TelNumber>
        //  <Portrait>dd</Portrait>
        //</PersonalInfo>
        public static bool Parse(XElement info)
        {
            try
            {
                UserId = Guid.Parse(info.Attribute("GUID").Value);
                RegEmail = info.Attribute("RegEmail").Value;
                if (info.Element("Nickname") != null) { Nickname = info.Element("Nickname").Value; }
                if (info.Element("EmailAddress") != null) { EmailAddress = info.Element("EmailAddress").Value; }
                if (info.Element("TelNumber") != null) { TelNumber = info.Element("TelNumber").Value; }
                if (info.Element("Portrait") != null)
                {
                    Portrait = int.Parse(info.Element("Portrait").Value);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static void OnLogout()
        {
            Nickname = null;
            EmailAddress = null;
            TelNumber = null;
            UserId = Guid.Empty;
            RegEmail = null;
        }
    }
}
