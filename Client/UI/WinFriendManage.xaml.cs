﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Interop;

namespace P2PChat.Client.UI
{
    /// <summary>
    /// WinFriendManage.xaml 的交互逻辑
    /// </summary>
    public partial class WinFriendManage : Window
    {
        public WinFriendManage()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            friendList_Entry1.Portrait = Imaging.CreateBitmapSourceFromHBitmap(
                Properties.Resources.Portrait_02.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            friendList_Entry2.Portrait = Imaging.CreateBitmapSourceFromHBitmap(
                Properties.Resources.Portrait_03.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            friendList_Entry3.Portrait = Imaging.CreateBitmapSourceFromHBitmap(
                Properties.Resources.Portrait_03.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            
        }
    }
}
