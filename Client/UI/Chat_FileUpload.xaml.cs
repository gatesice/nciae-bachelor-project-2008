﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Xml.Linq;
using P2PChat.Net;

namespace P2PChat.Client.UI
{
    /// <summary>
    /// Chat_FileUpload.xaml 的交互逻辑
    /// </summary>
    public partial class Chat_FileUpload : UserControl
    {
        public Chat_FileUpload()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        // 发送文件
        private void SendFile(Socket netSocket)
        {
            // 创建发送等待

            PacketParser p = new PacketParser();
            XElement xml = new XElement("Packet",
                new XElement("Function", "SendFile"),
                new XElement("ListeningPort", 0));
            byte[] packetContent = p.PackContent(PacketType.RawXmlString, "program_SENDFILE");
            try
            {
                // 发送消息
                netSocket.Send(packetContent);

                // 接收数据
                // TODO
            }
            catch (Exception ex) 
            {

            }
            finally
            {

            }
        }
    }
}
