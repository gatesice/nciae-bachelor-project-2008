﻿/**
 * IHashable.cs
 * 
 * 作者：Gates_ice
 * 日期：2012年1月18日
 * 说明：
 * 为哈希计算方法提供接口方法
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace P2PChat.Security.Hasher
{
    /// <summary>
    /// 为哈希的计算提供接口方法
    /// </summary>
    public interface IHashable
    {
        byte[] ComputeHash(byte[] data);
        byte[] ComputeHash(Stream data);

        byte[] ComputeHash(byte[] data, out string result);
        byte[] ComputeHash(Stream data, out string result);
    }
}
