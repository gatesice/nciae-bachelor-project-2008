﻿/**
 * SHA1Hash.cs
 * 
 * 作者：Gates_ice
 * 日期：2012年1月18日
 * 说明：
 * 使用SHA1算法实现哈希计算
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Security.Hasher
{
    /// <summary>
    /// 使用SHA1算法对数据进行哈希运算。
    /// </summary>
    public class SHA1Hash : Hash
    {
        /// <summary>
        /// 初始化对象
        /// </summary>
        public SHA1Hash()
            : base("SHA1")
        {

        }

        /// <summary>
        /// 计算字节数组的哈希值。
        /// </summary>
        /// <param name="data">哈希的数据</param>
        /// <returns>哈希值的字节数组</returns>
        public override byte[] ComputeHash(byte[] data)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create();
            byte[] byteResult = sha1.ComputeHash(data);
            return byteResult;
        }
        /// <summary>
        /// 计算一段数据流的哈希值。
        /// </summary>
        /// <param name="data">哈希的数据</param>
        /// <returns>哈希值的字节数组</returns>
        public override byte[] ComputeHash(System.IO.Stream data)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create();
            byte[] byteResult = sha1.ComputeHash(data);
            return byteResult;
        }

        /// <summary>
        /// 计算字节数组的哈希值。
        /// </summary>
        /// <param name="data">哈希的数据</param>
        /// <param name="result">传出哈希值的字符串表达</param>
        /// <returns>哈希值的字节数组</returns>
        public override byte[] ComputeHash(byte[] data, out string result)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create();
            byte[] byteResult = sha1.ComputeHash(data);
            result = ByteArrayToHexString(byteResult);
            return byteResult;
        }
        /// <summary>
        /// 计算一段数据流的哈希值。
        /// </summary>
        /// <param name="data">哈希的数据</param>
        /// <param name="result">传出哈希值的字符串表达</param>
        /// <returns>哈希值的字节数组</returns>
        public override byte[] ComputeHash(System.IO.Stream data, out string result)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create();
            byte[] byteResult = sha1.ComputeHash(data);
            result = ByteArrayToHexString(byteResult);
            return byteResult;
        }
    }
}
